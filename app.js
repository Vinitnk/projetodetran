var	fs = require('fs');
var horseman = require('node-horseman');

var dados = [
	{"placa":'PWW0477', "renavam":"1067558745"},
	{"placa":"PYT7634", "renavam":"1104687736"},
	{"placa":"PYT7678", "renavam":"1104688252"},
	{"placa":"PXO6131" , "renavam":"1082149702"}
];//fim do dados

var extrato = function(dados){
	// body...
	console.log(dados.placa);
	dados.forEach((dados) => {
		var horse = new horseman();
			horse
				.open("https://www.detran.mg.gov.br/veiculos/situacao-do-veiculo/emissao-de-extrato-de-multas")//abrir pagina
				.type('#EmitirExtratoMultaPlaca', dados.placa)//input da placa
				.type('#EmitirExtratoMultaRenavam', dados.renavam)//input do renavam
				.screenshot('dados.png')
				.click("button:contains('Avançar')")//click do botao
		  		// .keyboardEvent('keypress', 16777221)//enter do teclado
				.waitForSelector('#flashMessage')//espera div com mensagem
				.text('#flashMessage')//pega o texto da div com id flashMessage
				.then((text)=>{//depois de pegar o texto add no arquivo de resultado.
					//console.log('resultado.json',"{placa: "+dados.placa+", "+"renavam: "+ dados.renavam +", "+"Situação: "+ text+"}\n");
				fs.appendFile('resultado.json',"{placa: "+dados.placa+", "+"renavam: "
					+ dados.renavam +", "+"Situação: "+ text+"}\n");// add resultado no arquivo.json
				})//fim do then().
				.close()
	})//fim do foreach
};//fim da function
setTimeout(extrato(dados), 5000);